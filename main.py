import os
import numpy as np
import cv2
import torch

from services import calculate_blur_var, load_image, load_model, get_grounding_output, draw_segment_on_image


def Dino(image_path):
    config_file = "groundingdino/config/GroundingDINO_SwinT_OGC.py"  # change the path of the model config file
    checkpoint_path = "weights/groundingdino_swint_ogc.pth"  # change the path of the model
    image_path = image_path
    text_prompt = "pole, pole tag, antenna, tape"
    box_threshold = 0.3
    text_threshold = 0.25
    cpu_only = True

    # load image
    image_pil, image = load_image(image_path)
    # load model
    model = load_model(config_file, checkpoint_path, cpu_only=cpu_only)

    # run model
    boxes_filt, pred_phrases = get_grounding_output(
        model, image, text_prompt, box_threshold, text_threshold, cpu_only=cpu_only
    )

    # visualize pred
    size = image_pil.size
    pred_dict = {
        "boxes": boxes_filt,
        "size": [size[1], size[0]],  # H,W
        "labels": pred_phrases,
    }
    return pred_dict


def SAM(pred_dict, image_path):
    out = draw_segment_on_image(pred_dict, image_path)
    cv2.imwrite(os.path.join("pred.jpg"), out)


def check_blur(image_path, pred_dict, th=100):
    H, W = pred_dict["size"]
    boxes = pred_dict["boxes"]
    labels = pred_dict["labels"]

    image = cv2.imread(image_path)
    complete_blur_var = calculate_blur_var(image)
    vars = []
    for box, label in zip(boxes, labels):
        box = box * torch.Tensor([W, H, W, H])
        box[:2] -= box[2:] / 2
        box[2:] += box[:2]
        x0, y0, x1, y1 = box
        x0, y0, x1, y1 = int(x0), int(y0), int(x1), int(y1)

        image = cv2.imread(image_path)
        image = image[y0:y1, x0:x1]
        blur_var = calculate_blur_var(image)
        vars.append(blur_var)
    # Normalize the variance to a score between 0 and 100
    # score = (blur_var / th) * 100

    print("Complete image blur var: ", complete_blur_var)
    print("Avg of Detected area blur var: ", sum(vars) / len(vars))


def check_contrast(image_path):
    # Setting Contrast Threshold
    contrast_threshold = 50.0

    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    contrast = np.std(gray)
    if contrast < contrast_threshold:
        print("Image has less contrast than threshold",contrast)
    else:
        print("Image has exceeded contrast threshold",contrast)
    

def check_brightness(image_path):
    # Setting Sharpness Threshold
    sharpness_threshold = 100.0

    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    sharpness = cv2.Laplacian(gray, cv2.CV_64F).var()
    if sharpness < sharpness_threshold:
        print("Image has less contrast than threshold",sharpness)
    else:
        print("Image has exceeded contrast threshold",sharpness)



if __name__ == "__main__":
    image_path = 'dataset/1.jpg'

    pred_dict = Dino(image_path)
    check_blur(image_path, pred_dict, th=150)
    check_contrast(image_path)
    check_brightness(image_path)
    SAM(pred_dict, image_path)
