import numpy as np
import torch
import matplotlib.pyplot as plt
import cv2

input_box = np.array([425, 600, 700, 875])
input_box = np.array([425, 600, 700, 875])

from segment_anything import sam_model_registry, SamPredictor

def mask_image(mask, image):
    image = cv2.imread('test3.png')
    color = np.array([0,255,0], dtype='uint8')
    masked_img = np.where(mask[...,None], color, image)
    out = cv2.addWeighted(image, 0.8, masked_img, 0.2,0)
    return out

sam_checkpoint = "sam_vit_h_4b8939.pth"
model_type = "vit_h"
device = "cuda"
sam = sam_model_registry[model_type](checkpoint=sam_checkpoint)
sam.to(device=device)
image = cv2.imread('test3.png')
image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

predictor = SamPredictor(sam)
predictor.set_image(image)

input_box = np.array([663, 1143, 1626, 1772])

masks, _, _ = predictor.predict(
    point_coords=None,
    point_labels=None,
    box=input_box[None, :],
    multimask_output=False,
)

cv2.imwrite('ab.jpg', mask_image(masks[0], "test3.png"))